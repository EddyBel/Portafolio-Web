import { FrontImg } from "./index.front";
import { MainText } from "./index.text";
import { Proyects } from "./index.proyects";
import { IndexAbout } from "./index.about";

export { FrontImg, MainText, Proyects, IndexAbout };
