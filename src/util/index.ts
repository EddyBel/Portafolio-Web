import { generateText } from "./generate";
import { typingText, incrementValue, changeStringInfinite } from "./render";
import { createLargeString, trimTheString } from "./text";
import {
  stringToArray,
  stringToStar,
  validateToDataFromDict,
  getExtension,
} from "./format";

export {
  generateText,
  typingText,
  incrementValue,
  changeStringInfinite,
  trimTheString,
  createLargeString,
  stringToArray,
  stringToStar,
  validateToDataFromDict,
  getExtension,
};
